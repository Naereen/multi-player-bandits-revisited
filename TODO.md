# TODO

## Clean up
- [ ] Clean up `old_tex/`
- [ ] Clean up `Julia_Code/`


## arXiv & HAL version
- [ ] Use the `abstract` class and not the JMLR format
- [ ] Send it on HAL, let it automatically upload to arXiv (or use OverLeaf auto upload feature)

## Longer version?
- If we have ideas on what to add...
