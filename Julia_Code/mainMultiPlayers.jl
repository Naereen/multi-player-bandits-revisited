#!/usr/bin/env julia

using HDF5
using PyPlot


@everywhere include("AlgosMP.jl")

# BANDIT INSTANCE

@everywhere mu = round.(sort(rand(6), rev = true), 2)
@everywhere M = 2

# HORIZON AND REPETITIONS
T = 5000 # horizon
NbRuns = 500 # nb of Monte Carlo simulations

# POLICIES

@everywhere RhoRandRew(M, Table) = RhoRand(M, Table, "Off", "reward", "KLUCB")
@everywhere RhoRandBar(M, Table) = RhoRand(M, Table, "Off", "intermediate", "KLUCB")
@everywhere SelfishBar(M, Table) = Selfish(M, Table, "Off", "intermediate", "KLUCB")
@everywhere CUCBRandRew(M, Table) = CUCBRand(M, Table, "Off", "reward", "KLUCB")

@everywhere Algos = [RhoRand, RhoRandRew, RhoRandBar, Selfish, SelfishBar, CUCBRand]
@everywhere nbAlgos = length(Algos)

# Name
fname = "RandomInstance_"

Fig = "Save"

print("mu is $(mu) and M is $(M)\n")

@everywhere K = length(mu)
@everywhere mumax = sum(mu[1:M])

nbstep = 200
tsave = floor.(Int, linspace(1, T,nbstep))'


@everywhere function RunExp(Algos, mu, M,T, NbRuns)
    @everywhere product = K * M
    Regret, Draws, Collisions = @parallel ((x, y) -> (vcat(x[1], y[1]), x[2] + y[2], x[3] + y[3]))
    for n in 1:NbRuns
        Table = DrawTable(mu, T)
        regret = zeros(1, nbAlgos * nbstep)
        N = zeros(nbAlgos, product)
        C = zeros(nbAlgos, product)
        for i in 1:nbAlgos
            algorithm = Algos[i]
        rews, tabN, tabC, collisions, chosenarms, GE1, GE2 = algorithm(M, Table)
            N[i, :] += vec(reshape(tabN, 1,product))
            C[i, :] += vec(reshape(tabC, 1,product))
        regret[((i - 1) * nbstep + 1):i * nbstep] = mumax * tsave - sum(cumsum(rews, 2), 1)[tsave]
    end
        # display
        if (mod(n, NbRuns / 10) == 0)
            print("$(10) % done\n")
        end
        regret, N,C
    end
    clf()
    # using the stored information to display what we want
    for i in 1:nbAlgos
        NbDraws = reshape(Draws[i, :], M,K) / NbRuns
    NbCollisions = reshape(Collisions[i, :], M,K) / NbRuns
        Reg = mean(Regret[:, ((i - 1) * nbstep + 1):i * nbstep], 1)
        print("Results for $(Algos[i]), averaged over $(NbRuns) repetitions: \n")
        print("The number of draws is \n")
        print("$(floor.(Int, NbDraws))\n")
    print("The number of collisions is \n")
        print("$(round.(NbCollisions, 2))\n")
        plot(tsave', Reg', label = "$(Algos[i])")
        print("Regret curve: see plot \n\n")
    end
    xlabel("t")
    ylabel("Mean regret")
    title("Regret up to T=$(T) averaged over $(NbRuns) simulations \n mu=$(mu) and M=$(M)")
    legend(loc = "upper left")
    if (Fig == "Save")
    RandNb = round(Int, 1000000 * rand())
    savefig("figures / NewAlgos_K_$(K)_M_$(M)_$(RandNb).pdf")
    end
end


@everywhere function RunExpSave(Algos, mu, M,T, NbRuns)
    @everywhere product = K * M
    Regret, Draws, Collisions = @parallel ((x, y) -> (vcat(x[1], y[1]), x[2] + y[2], x[3] + y[3]))
    for n in 1:NbRuns
        Table = DrawTable(mu, T)
        regret = zeros(1, nbAlgos * nbstep)
        N = zeros(nbAlgos, product)
        C = zeros(nbAlgos, product)
        for i in 1:nbAlgos
            algorithm = Algos[i]
            rews, tabN, tabC, collisions, chosenarms, GE1, GE2 = algorithm(M, Table)
            N[i, :] += vec(reshape(tabN, 1,product))
            C[i, :] += vec(reshape(tabC, 1,product))
        regret[((i - 1) * nbstep + 1):i * nbstep] = mumax * tsave - sum(cumsum(rews, 2), 1)[tsave]
    end
        # display
        if (mod(n, NbRuns / 10) == 0)
            print("$(10) % done\n")
        end
        regret, N,C
    end
    clf()
    # using the stored information to display what we want
    for i in 1:nbAlgos
        name = "results/$(fname)T_$(T)_N_$(NbRuns)_$(Algos[i]).h5"
        h5write(name, "mu", mu)
        h5write(name, "tsave", tsave)

        NbDraws = reshape(Draws[i, :], M,K) / NbRuns
        h5write(name, "NbDraws", NbDraws)

        NbCollisions = reshape(Collisions[i, :], M,K) / NbRuns
        h5write(name, "NbCollisions", NbCollisions)


        Reg = Regret[:, ((i - 1) * nbstep + 1):i * nbstep]
        h5write(name, "Regret", Reg)
        Reg = mean(Reg, 1)

        print("Results for $(Algos[i]), averaged over $(NbRuns) repetitions: \n")
        print("The number of draws is \n")
        print("$(floor.(Int, NbDraws))\n")
        print("The number of collisions is \n")
        print("$(floor.(Int, NbCollisions))\n")
        plot(tsave', Reg', label = "$(Algos[i])")
        print("Regret curve: see plot \n\n")
    end
    xlabel("t")
    ylabel("Mean regret")
    title("Regret up to T=$(T) averaged over $(NbRuns) simulations")
    legend(loc = "upper left")
end


@everywhere function CheckBehavior(algorithm, mu, M,T, NbRuns)
    TPrime, TPrimeAlt, Miss = @parallel ((x, y) -> (x[1] + y[1], x[2] + y[2], x[3] + y[3]))
    for s =1:NbRuns
        Table = DrawTable(mu, T)
        Rewards, N,C, Collisions, ChosenArms, GoodEvent, GoodEvent2 = algorithm(M, Table)
        tPrime = cumsum((sum(GoodEvent, 1) .< M), 2)
        tPrimeAlt = cumsum((sum(GoodEvent2, 1) .< M), 2)
        miss = zeros(1, M)
        for k = 1:M
            Missing = cumsum(reshape([!(k in ChosenArms[:, t]) for t = 1:T], 1,T))
            miss[k] = sum(Missing)
        end
        tPrime, tPrimeAlt, miss
    end
    print("Results for $(algorithm) for horizon T= $(T), averaged over N=$(NbRuns) simulations \n")
    print("The value of T' is $(TPrime[T] / NbRuns) \n")
    print("The value of T'bis is $(TPrimeAlt[T] / NbRuns) \n")
    for k = 1:M
        print("The number of rounds in which arm $(k) is missing from the chosen arms is $(Miss[k] / NbRuns) \n")
    end
end

# Run all this
RunExp(Algos, mu, M,T, NbRuns)

@everywhere M = 3
@everywhere mumax = sum(mu[1:M])
RunExp(Algos, mu, M,T, NbRuns)

@everywhere M = 4
@everywhere mumax = sum(mu[1:M])
RunExp(Algos, mu, M,T, NbRuns)

@everywhere M = 5
@everywhere mumax = sum(mu[1:M])
RunExp(Algos, mu, M,T, NbRuns)

@everywhere M = 6
@everywhere mumax = sum(mu[1:M])
RunExp(Algos, mu, M,T, NbRuns)
