The number of arms $K$, the number of player $M$ and the
  problem $\boldsymbol{\mu}$ are fixed, but $\boldsymbol{\mu}$ is unknown to the players.
  %
  The proof relies on Lemma~\ref{lem:DecompositionRegret}, and in fact we lower-bound only the first of the three terms, as the two other terms are non-negative.
  The goal is therefore to lower bound the term
  $\sum_{j=1}^{M} \sum_{k \in \Mworst} (\mu_M^* -  \mu_k) \E[T^j_k(T)]$,
  and so to control the selections of the worst arms, $\E[T^j_k(T)]$ if $k \in \Mworst$, exactly like it is done for the single-player case or the centralized case.

  Fix a arm index $k \in \{1, \dots, K\}$.
  %
  Let $\varepsilon > 0$, such that
  $\varepsilon < \mu_M^* - \mu_{{M-1}}^*$ (as $\boldsymbol{\mu}\in\cP_M$).
  %
  Let us build a problem $\lambda$, very similar to $\boldsymbol{\mu}$ but
  different on one arm: $\forall k', \lambda_{k'} = \mu_{k'}$,
  except for $\lambda_k = \mu_M^* + \varepsilon$.
  %
  So with the star index notation, we have
  $\lambda_{1^*} = \mu_{1}^*, \dots \lambda_{{M-1}^*} = \mu_{{M-1}}^*, \lambda_{M^*} = \lambda_k > \mu_M^*$.

  Fix the player index $j \in \{1, \dots, M\}$. The goal is
  essentially to prove that $\E_{\boldsymbol{\mu}}[T^j_k(T)]$ is small and
  $\E_{\lambda}[T^j_k(T)]$ is big.
  %
  We consider the observations $X_1, \dots, X_T$, seen as
  $X_t = X_{A^j(t), t}$ where $(X_{a,t})_{a,t}$ is an \emph{i.i.d.}
  stream. They can also be seen as $X_t = Y_{A^j(t), N_{A^j(t)}(t) + 1}$
  is $(Y_{a,s})_{a,s}$ is another stream, indexed by
  $s = N_{A^j(t)}(t) + 1$, for $N_{a}(t)$ the number of pulls of arm
  $a$ for times $< t$ by player $j$. And so we also have
  $N_k(t) = T^j_k(t)$.

  Let $\mathcal{L}$ design the \emph{likelihood} of the observations,
  under problem $\boldsymbol{\mu}$ or $\lambda$. By independence,
  $\mathcal{L}_{\boldsymbol{\mu}}(X_1,\dots,X_T) = \prod_{t=1}^{T} \mathcal{L}_{\boldsymbol{\mu}}(X_t) = \prod_{t=1}^{T} f_{\mu_{A_t}}(X_t)$,
  if $f_{\mu_k}(X)$ denote the distribution of the $k-th$ arm, parametrized by $\mu_k$.
  For Bernoulli arms, $f_{\boldsymbol{\mu}}(x) = \boldsymbol{\mu}^{x} (1 - \boldsymbol{\mu})^{1 - x}$.
  %
  One way to write the likelihood is therefore
  $\mathcal{L}_{\boldsymbol{\mu}}(X_1,\dots,X_T) = \prod_{t=1}^{T} f_{\mu_{A_t}}(X_t)$,
  but thanks to $X_t = Y_{A^j(t), N_{A^j(t)}(t) + 1}$, we can also write
  it as
  $\mathcal{L}_{\boldsymbol{\mu}}(X_1,\dots,X_T) = \prod_{a=1}^{K} \prod_{s=1}^{N_a(T)} f_{\mu_{a}}(Y_{a,s})$.
  %
  The log-likelihood ratio between problems
  $\lambda$ and $\boldsymbol{\mu}$, denoted $L_T(\boldsymbol{\mu}, \lambda)$,
  for observations $X_1, \dots, X_T$ is
  \vspace*{-5pt}  % XXX
  \begin{align*}
  L_T(\boldsymbol{\mu}, \lambda)
    &= \log \frac{\mathcal{L}_{\boldsymbol{\mu}}(X_1,\dots,X_T)}{\mathcal{L}_{\lambda}(X_1,\dots,X_T)}
    = \sum_{a=1}^{K} \sum_{s=1}^{N_a(T)} \log \frac{f_{\mu_{a}}(Y_{a,s})}{f_{\lambda_{a}}(Y_{a,s})}.
    \shortintertext{But for $a \neq k$, $\mu_a = \lambda_a$, so only one term in the $\log$ is not $1$,}
    &= \sum_{s=1}^{N_k(T)} \log \frac{f_{\mu_k}(Y_{k,s})}{f_{\lambda_k}(Y_{k,s})}
    = \sum_{s=1}^{N_k(T)} \log \frac{f_{\mu_k}(Y_{k,s})}{f_{\mu_M^* + \varepsilon}(Y_{k,s})}.
  \end{align*}
  %
  For one term, we have by definition of the Kullback-Leibler divergence,
  $\E_{\boldsymbol{\mu}}[ \log \frac{f_{\mu_k}(Y_{k,s})}{f_{\mu_M^* + \varepsilon}(Y_{k,s})} ] = \kl(\mu_k, \mu_M^* + \varepsilon)$,
  as $Y_{k,s}$ are \emph{i.i.d.} wrt to $s$.
  %
  Now, for the sum on $s = 1, \dots, N_k(T) = 1, \dots, T^j_k(T)$, we
  need to justify that the upper bound $T^j_k(T)$ is a
  % \href{https://en.wikipedia.org/wiki/Stopping\_time}{}
  stopping time with respect to
  % http://www.bramdejonge.nl/pdf/stoppingtimes.pdf
  $Y_{k,s}$. By definition of the streams, $Y_{k,s}$
  is independent of $T^j_k(T)$, so
  % \href{https://en.wikipedia.org/wiki/Wald\%27s\_equation\#General\_version}{}
  Wald's Lemma (\cite{Wald45})
  can be applied and so
  $$
    \E_{\boldsymbol{\mu}} \left[ \sum_{s=1}^{N_k(T)} \log \frac{f_{\mu_k}(Y_{k,s})}{f_{\mu_M^* + \varepsilon}(Y_{k,s})} \right]
    = \E_{\boldsymbol{\mu}}[N_k(T)] \E_{\boldsymbol{\mu}}\left[ \log \frac{f_{\mu_k}(Y_{k,s})}{f_{\mu_M^* + \varepsilon}(Y_{k,s})} \right]
    = \E_{\boldsymbol{\mu}}[T^j_k(T)] \kl(\mu_k, \mu_M^* + \varepsilon).
  $$

  Finally, we have,
  \begin{equation}\label{eq:Wald}
    \E_{\boldsymbol{\mu}}[L_T(\boldsymbol{\mu}, \lambda)] = \E_{\boldsymbol{\mu}}[T^j_k(T)] \kl(\mu_k, \mu_M^* + \varepsilon).
  \end{equation}
  %
  If we use equation \eqref{eq:Wald} and Lemma~\ref{lem:changeOfLaws} (see Appendix~\ref{app:changeOfLaws}), then for any measurable event $A$:
  $$ \E_{\boldsymbol{\mu}}[T^j_k(T)] \kl(\mu_k, \mu_M^* + \varepsilon) \geq \kl(\Pr_{\boldsymbol{\mu}}(A), \Pr_{\lambda}(A)). $$
  The goal of this proof is to lower bound $\E[T^j_k(T)]$, and
  this inequality does the job.
  %
  We need to \emph{craft} an event $A$ such that $\Pr_{\boldsymbol{\mu}}(A)$ is very
  high (almost $1$), and $\Pr_{\lambda}(A)$ is very low (or the
  converse), in order for this term $\kl(\Pr_{\boldsymbol{\mu}}(A), \Pr_{\lambda}(A))$
  to be lower bounded by $\log(T)$.

  Let $g(T)$ be a function of $T$ such that
  $\frac{T}{M} > g(T) \gg \log(T)$, for instance $g(T) = \frac{T}{2M}$
  or $g(T) = \sqrt{\frac{T}{M}}$. We prefer to consider
  $g(T) = \frac{T}{2M}$ explicitly.
  %
  And consider the event $A = \{ T^j_k(T) \geq g(T) \}$. By
  construction, $k \in \Mworst(\boldsymbol{\mu})$ and $k \in \Mbest(\lambda)$.
  %
  %
  \begin{enumerate}\tightlist
  \def\labelenumi{\arabic{enumi}.}
  \item
    Upper-bounding $\Pr_{\boldsymbol{\mu}}(A)$ is done with Markov's inequality,
    $\Pr_{\boldsymbol{\mu}}(A) \leq \frac{2M}{T} \E_{\boldsymbol{\mu}}(T_{k,t}(T))$, but for
    $k \in \Mworst(\boldsymbol{\mu})$, by the uniform efficiency hypothesis on
    $\rho$, we have $\E_{\boldsymbol{\mu}}[T_{k,t}(T)] = o(T^{\alpha})$, so
    $\Pr_{\boldsymbol{\mu}}(A) = o(T^{\alpha-1})$.
  \item
    And lower-bounding $\Pr_{\lambda}(A)$ for $\lambda$,
    $k \in \Mbest(\lambda)$ is done by considering the complementary event
    of $A$, $A^c$, for which
    $\Pr_{\lambda}(A^c) = \Pr_{\lambda}(\frac{T}{M} - T^j_k(T) > \frac{T}{M} - g(T)) = \Pr_{\lambda}(\frac{T}{M} - T^j_k(T) > \frac{T}{2M})$,
    so Markov inequality can also be applied, giving
    $\Pr_{\lambda}(A^c) \leq \frac{2M}{T} \E_{\lambda}[\frac{T}{M} - T^j_k(T)] = o(T^{\alpha-1})$.
  \end{enumerate}
  %
  % \todo[inline]{FIXME shorten this part ?!}
  Lemma~\ref{lem:eqKLbin} can be used to bound the binary KL divergence
  $\kl$.
  %
  And we also have $\kl(x, y) = \kl(1 - x, 1 - y)$. So if we define
  $x = \Pr_{\boldsymbol{\mu}}(A) \mathop{\to}\limits_{T \to +\infty} 0$, and
  $y = \Pr_{\lambda}(A) \mathop{\sim}\limits_{T \to +\infty} 1$, we have
  $1 - y = \Pr_{\lambda}(A^c) \leq \frac{2M}{T} \E_{\lambda}[\frac{T}{M} - T^j_k(T)]$,
  and so
  $$
    \frac{\kl(1 - x, 1 - y)}{\log T}
    \geq \frac{(1-x)\log(\frac{1}{1-y})}{\log T} - \underbrace{\frac{\log 2}{\log T}}_{\mathop{\to}\limits_{T \to +\infty} 0}.
  $$

  And $(1-x) \mathop{\sim}\limits_{x \to 0} 1$, so let us focus on the
  term $\frac{\log(\frac{1}{1-y})}{\log T}$,
  %
  \begin{align*}
    \frac{\log(\frac{1}{1-y})}{\log T}
    &= \frac{- \log(2M) + \log(T) - \log(\underbrace{\E_{\lambda}\left[\frac{T}{M} - T^j_k(T)\right]}_{:= h(T)})}{\log T} \\
    &= 1 - \underbrace{\frac{\log(2M)}{\log T}}_{\mathop{\to}\limits_{T \to +\infty} 0} - \frac{\log(h(T))}{\log T}
    \shortintertext{But $h(T) = o(T^{\alpha})$ so for any $\eta > 0$, $h(T) \leq \eta T^{\alpha}$. So $\frac{\log h(T)}{\log T} \leq \alpha + \frac{\log \eta}{\log T} \mathop{\to}\limits_{T \to +\infty} \alpha$, and so}
    \frac{\log(\frac{1}{1-y})}{\log T}
    &\mathop{\geq}\limits_{T \to +\infty} 1 - \alpha
  \end{align*}

  From the beginning, $\alpha > 0$ is a free parameter, as the \emph{uniform
  efficiency} hypothesis has to be true for all $\alpha$. We can make
  $\alpha$ tends to $0$, and so this gives $$
    \frac{\log(\frac{1}{1-y})}{\log T} \mathop{\geq}\limits_{T \to +\infty} 1.
  $$
  %
  Therefore, in terms of $\lim\inf$ for $T\to\infty$, this gives
  \begin{align}\label{eq:SubOptimalSelectionsLower}
    \frac{\E_{\boldsymbol{\mu}}[T^j_k(T)]}{\log T}
    &\geq \frac{\kl(\Pr_{\boldsymbol{\mu}}(A), \Pr_{\lambda}(A))}{\log T} \frac{1}{\kl(\mu_k, \mu_M^* + \varepsilon)} \notag\\
    &\mathop{\geq}\limits_{T \to +\infty} \frac{1}{\kl(\mu_k, \mu_M^* + \varepsilon)}.
  \end{align}
  %
  The parameter $\varepsilon > 0$ is also free, so we can make it tend to
  $0$, and by continuity of the Kullback-Leibler divergence, $$
    \kl(\mu_k, \mu_M^* + \varepsilon) \mathop{\to}\limits_{\varepsilon \to 0} \kl(\mu_k, \mu_M^*).
  $$

  \textbf{Conclusion:}
  we have the lower-bound in $\log T$, on the number of selections $\E_{\boldsymbol{\mu}}[T^j_k(T)]$,
  $$ \mathop{\lim\inf}\limits_{T \to +\infty} \frac{\E_{\boldsymbol{\mu}}[T^j_k(T)]}{\log T} \geq \frac{1}{\kl(\mu_k, \mu_M^*)}. $$
  %
  And so we have, as wanted, $$ \mathop{\lim\inf}\limits_{T \to +\infty}  \frac{R_T(\boldsymbol{\mu}, M, \rho)}{\log T} \geq M \times
  \left( \sum_{k\in \Mworst} \frac{(\mu_M^* -  \mu_k)}{\kl(\mu_k, \mu_M^*)} \right).
  $$
  % \vspace*{-25pt}
  % which has to be understood as a $\lim\inf$ for $T \to +\infty$.

\subsection{Lemmas used for the previous proof}

We give but do not prove two lemmas, used in the previous proof of Theorem~\ref{thm:BetterLowerBound}.

% -----------------------------------------------------------------
% \subsection{A useful lemma, ``Change of laws''}\label{app:changeOfLaws}

\begin{lemma}\label{lem:changeOfLaws}
  For any measurable event $A$,
  $\E_{\boldsymbol{\mu}}[L_T(\boldsymbol{\mu}, \lambda)] \geq \kl(\Pr_{\boldsymbol{\mu}}(A), \Pr_{\lambda}(A))$.
\end{lemma}
  \begin{proof}
    See for instance \cite[Lemma 1.3]{Kaufmann12PhD}.
    \vspace*{-15pt}
  \end{proof}


% -----------------------------------------------------------------
% \subsection{A lower-bound for the binary Kullback-Leibler divergence}\label{app:}

% This other lemma was also used in the proof of Theorem~\ref{thm:BetterLowerBound}.
This lower-bound for the binary Kullback-Leibler divergence was also used.

\begin{lemma}\label{lem:eqKLbin}
  The binary Kullback-Leibler $\kl$ divergence satisfies
    \vspace*{-5pt}
    \begin{equation}
    \forall x, y \in (0,1), \;\;
    \kl(x, y)
    % = x \log\left(\frac{x}{y}\right) + (1 - x) \log\left(\frac{1 - x}{1 - y}\right)
    \geq x \log\left(1/y\right) - \log(2).
  \end{equation}
\end{lemma}
  \begin{proof}
    See for instance \cite{Garivier16}.
    \vspace*{-15pt}
  \end{proof}
% \begin{proof}
% First, let prove that $\forall x\in(0,1), x \log x \geq - \frac{\mathrm{e}^{-1}}{\ln(10)}$.
% $f(x) := x \ln x$ is of class $\Ccal^1$ and $f'(x) = \ln(x) - 1$ so $f$ is convex on $(0,1)$ and so is minimal at $x_0$ such that $f'(x_0) = 0$, and $x_0 = \mathrm{e}^{-1}$.
% So $f(x) \geq f(x_0) = -\mathrm{e}^{-1}$.
% %
% Now, $\kl(x, y) = x \log x + x \log\left(\frac{1}{y}\right) + (1 - x) \log(1-x) + (1-x)\log\left(\frac{1}{1 - y}\right)$, and the two terms $x \log x$ and $(1-x) \log(1-x)$ are lower-bounded by $\frac{-\mathrm{e}^{-1}}{\ln(10)}$.
% The forth term $(1-x)\log\left(\frac{1}{1 - y}\right)$ is non-negative, as $(1-x) \geq 0$ and $\frac{1}{1 - y} > 1$.
% %
% Finally, we have $\kl(x, y) \geq x \log\left(\frac{1}{y}\right) + 2 \frac{-\mathrm{e}^{-1}}{\ln(10)} $.
% And numerically, $-2 \frac{\mathrm{e}^{-1}}{\ln(10)} \simeq - 0.319 > -0.683 \simeq - \log(2)$.
% So $\kl(x, y) \geq x \log\left(\frac{1}{y}\right) - \log(2)$ as wanted.
% \end{proof}


% -----------------------------------------------------------------
% \subsection{Chernoff bounds for \klUCB{} confidence intervals}\label{app:ChernoffKL}

We give a last Lemma, used in the proof of Lemma~\ref{lem:SubOptimalSelections},
for Chernoff bounds for \klUCB{} confidence intervals.

\begin{lemma}\label{lem:ChernoffKL}
  If arm $m$ was selected $s$ times,
  then for any $\delta\in(0,1)$, we have
  \begin{align}\label{eq:ChernoffKL}
    \Pr\left[ s \times \kl^+(\widehat{\mu}_{m,s}, \mu_m) \geq \log(1 / \delta) \right]
      &\leq \delta,\\
    \Pr\left[ s \times \kl^-(\widehat{\mu}_{m,s}, \mu_m) \geq \log(1 / \delta) \right]
      &\leq \delta.
  \end{align}
  Where $\kl^+(x, y) := \kl(x, y) \times \indic(x \geq y)$
  and $\kl^-(x, y) := \kl(x, y) \times \indic(x \leq y)$.
\end{lemma}
  \begin{proof}
    % See for instance \cite[Lemma 1.3]{Kaufmann12PhD}.
    With the binary \kl{} divergence, Chernoff bounds are
    \begin{align*}
      \Pr\left[ \widehat{\mu}_{m,s} \geq x | x > \mu_m \right]
        &\leq \exp(- s \times \kl(x, \mu_m)) \\
      \Pr\left[ \widehat{\mu}_{m,s} \leq x | x < \mu_m \right]
        &\leq \exp(- s \times \kl(x, \mu_m)).
    \end{align*}
    The two results are symmetric, let us focus on the first one for $\kl^+$.
    By definition of $\kl^+$,
    letting $x = \widehat{\mu}_{m,s}$ and $\delta=\exp(- s \times \kl(\mu_m, x))$
    gives $\log(1 / \delta) = s \times \kl(\widehat{\mu}_{m,s}, \mu_m)$
    \begin{align*}
      \Pr\left[ s \times \kl^+(\widehat{\mu}_{m,s}, \mu_m) \geq \log(1 / \delta) \right]
        &\leq \delta.
    \end{align*}
    \vspace*{-40pt}
  \end{proof}
