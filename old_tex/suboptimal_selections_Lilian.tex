\begin{align*}
      \shortintertext{The $g_{m^*}(t)$ are KL upper-confidence bounds, and a useful result from the analysis of single-play \klUCB{} is that $\sum_{t=1}^T \Pr\left[ g_{1^*}(t) < \mu_1^* \right] = \smallO{\log(T)}$ (\ie, for the best arm), see \cite{Garivier11KL}. A similar result can be obtained for the multi-player case, for user $j$ and any of the $\Mbest$ arms, and so the second term is negligible when summing on $m=1,\dots,M$.}
      \E[T_k^j(T)]
      &\leq
      \underbrace{\left(\sum_{t=1}^T
        \Pr\left[ A^j(t) = k,\; \mu_{M^*} \leq g_k(t) \right] \right)}_{:= (\spadesuit)}
      +
      \smallO{\log(T)}.
    \end{align*}
    %
    For the more general case of \klUCB, the indexes for user $j$ are defined by $g^j_k(t) = g_k(t) = g_{k,N_k(t)}$,
    if $\hat{\mu}_{k,s} = (\sum_{t=1,A^j(t)=k,N_k(t) \leq s}^{\infty} X_k(t)) / s$ is the mean reward after the $s$ first selections of arm $k$,
    and
    $g_{k,s}(t) := \sup_{q \in [0, 1]} \left\{ q : \kl(\hat{\mu}_{k,s}, q) \leq \log(t) / s \right\}$.
    % with a constant $\alpha_j > 0$, usually set to $\alpha_j = 1$.
    We can therefore explicit the left term $(\spadesuit)$.
    %
    \begin{align*}
      % \sum_{t=1}^T  \Pr\left[ A^j(t) = k,\; \mu_{M^*} < g_k(t) \right]
      (\spadesuit) &=
      \E\left[ \sum_{t=1}^T  \mathbbm{1}\left( A^j(t) = k,\; \mu_{M^*} \leq g_k(t) \right) \right] \\
      &=
      \E\left[ \sum_{t=1}^T \sum_{s=1}^{\infty}  \mathbbm{1}\left( A^j(t) = k,\; \mu_{M^*} \leq g_{k,N_k(t)}(t), N_k(t) = s \right) \right].
      %
      \shortintertext{But $N_k(t) \leq t$, so $s \leq t$ and so the second sum on $s$ is finite. By definition of the index, $\mu_{M^*} < g_{k,s}(t)$ if and only if $\kl(\hat{\mu}_{k,s}(t), \mu_{M^*}) \leq \log(t) / s$ if $s = N_k(t)$. By inverting the two sums, we have,}
      &=
      \E\left[ \sum_{s=1}^T \sum_{t=s}^T  \mathbbm{1}\big( s \times \kl(\hat{\mu}_{k,s}(t), \mu_{M^*}) \leq \log(t) \big) \times \mathbbm{1}\big( A^j(t) = k, N_k(t) = s \big) \right].
      %
      \shortintertext{For tany $t\in\{1,\dots,T\}$, $\log(t) \leq \log(T)$, and so the first term can be bounded by a term independent of $t$,}
      &\leq
      \E\big[ \sum_{s=1}^T  \mathbbm{1}\big( s \times \kl(\hat{\mu}_{k,s}, \mu_{M^*}) \leq \log(T) \big) \times \underbrace{\left(\sum_{t=s}^T  \mathbbm{1}\left( A^j(t) = k, N_k(t) = s \right) \right)}_{\leq 1} \big].
      %
      \shortintertext{And the second term is small than $1$, because only one value of $t\in\{s,\dots,T\}$ satisfies $N_k(t) = s$ and $A^j(t)=k$ (when $s$ is fixed). So we have,}
      &\leq
      \sum_{s=1}^T  \Pr\left[ s \times \kl(\hat{\mu}_{k,s}, \mu_{M^*}) \leq \log(T) \right].
      %
      \shortintertext{Let $\varepsilon > 0$, define $S_T = (1+\varepsilon)\frac{\log(T)}{\kl(\mu_k, \mu_{M^*})}$, and split the sum on $s$ between $s \leq S_T$ and $s > S_T$,}
      &\leq
      \sum_{s=1}^{S_T}  \underbrace{\Pr\left[ \dots \right]}_{\leq 1}
      + \sum_{s=S_T+1}^T  \Pr\left[ s \times \kl(\hat{\mu}_{k,s}, \mu_{M^*}) \leq \log(T) \right].
      %
      \shortintertext{Because $s \geq S_T + 1$, we have $\Pr\left[ s \times \kl(\hat{\mu}_{k,s}, \mu_{M^*}) \leq \log(T) \right] \leq \Pr\left[ \kl(\hat{\mu}_{k,s}, \mu_{M^*}) \leq \frac{1}{1+\varepsilon} \kl(\mu_{k^*}, \mu_{M^*}) \right]$,}
      &\leq
      S_T
      + \sum_{s=S_T+1}^T  \Pr\left[ \kl(\hat{\mu}_{k,s}, \mu_{M^*}) \leq \frac{1}{1+\varepsilon} \kl(\mu_{k^*}, \mu_{M^*}) \right].
      %
      \shortintertext{Let $R(\varepsilon) > 0$ be such that $\kl(\mu_k + R(\varepsilon), \mu_{M^*}) \leq \frac{1}{1+\varepsilon} \kl(\mu_k, \mu_{M^*})$.
      Thanks to Lemma~\ref{lem:ChernoffKL}, this gives $\Pr\left[ \kl(\hat{\mu}_{k,s}, \mu_{M^*}) \leq \frac{1}{1+\varepsilon} \kl(\mu_{k^*}, \mu_{M^*}) \right] \leq \Pr\left[ \hat{\mu}_{k,s} > \mu_k + R(\varepsilon) \right] \leq \exp(- s R(\varepsilon)^2)$, and so for the sum,
      }
      &\leq
      S_T
      + \sum_{s=S_T+1}^T  \underbrace{\exp(- s R(\varepsilon)^2)}_{= (\exp(- R(\varepsilon)^2))^s}
      \leq
      S_T
      + \sum_{s=S_T+1}^{\infty}  (\exp(- R(\varepsilon)^2))^s.
      %
      \shortintertext{Indeed, $T < \infty$, and the series is convergent because it is the sum of a geometric series of reason $\exp(- R(\varepsilon)^2) < 1$ (as $R(\varepsilon)^2 > 0$). Its sum is only depending on $\varepsilon$, and is denoted $\sigma(\varepsilon) > 0$.
      Therefore, by expliciting $S_T$ we have an asymptotic upper-bound for this first term $(\spadesuit)$:}
      (\spadesuit) &\leq
      S_T + \sigma(\varepsilon)
      = (1 + \smallO{1}) \frac{\log(T)}{\kl(\mu_{k}, \mu_{M^*})} + \smallO{1}.
    \end{align*}
    %
    Finally %, by considering $\varepsilon \to 0$,
    we proved that asymptotically
    $$
      \E[T_k^j(T)]
      \leq \smallO{\log(T)} + (1 + \smallO{1}) \frac{\log(T)}{\kl(\mu_{k}, \mu_{M^*})} + \smallO{1}.
    $$
    In other words, in terms of an upper-bound with a superior limit ($\lim\sup$), we proved that any for player $j$ using \klUCB{} (\emph{for any rank selection procedure}) and any sub-optimal arm $k\in\Mworst$,
    its number of selection is asymptotically upper-bounded logarithmically:
    $$
      \mathop{\lim\sup}\limits_{T \to +\infty} \frac{\E_{\mu}[T_k^j(T)]}{\log T} \leq \frac{1}{\kl(\mu_k, \mu_M^*)}.
    $$
    % This upper-bound matches, for \rhoRandklUCB, the lower-bound \eqref{eq:SubOptimalSelectionsLower}, obtained on $\E[T_k^j(T)]$ in Theorem~\ref{thm:BetterLowerBound}.

    % We will use Lemmas~\ref{lem:ChernoffKL} and \ref{lem:lemFIXME2}, proved in Appendix~\ref{app:moreproofs}.
    % \todo[inline]{FIXME insert a drawing of the binary KL function, illustrating this value $R(\varepsilon)$.}

    Notice that in fact this proof turns out to be valid for \emph{any} rank selection process,
    as long as the rank $q^j(t)$ is a random variable, measurable with respect the past and the current observations,
    \ie, $\mathcal{F}_t$.
    \vspace*{-15pt}  % XXX remove if needed
